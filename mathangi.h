struct Node
{
	int data;
	struct Node *next;
};

class Llist
{	struct Node *head,*sorted;
	
	public:
		Llist();
		struct Node* getSorted();
		void setHead(struct Node* headV);
		struct Node* getHead();
		void display();
		Node* createNode(int value);
		void insertBegining();
		int length();
		int count(int value);
		int getNth(int pos);	
		void delList();
		int pop();
		void insertAtPos(int pos);
		void sortedInsert(struct Node* newN,struct Node *head);
		void sortedInsert(struct Node* newN);
		void insertionSort();
		void append(Llist list2);
		void frontBackSplit(Llist list1, Llist list2);
		void moveNodes(Llist list1);
		void removeDuplicates();
		void alternatingSplit(Llist listA, Llist listB);		
		void shuffleMerge(Llist listA, Llist listB);	
		Node* sortedMerge(struct Node *headA,struct Node *headB);
		Node* mergeSort(struct Node* head);
};

