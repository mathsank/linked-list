#include<iostream>
#include "mathangi.h"

using namespace std;

	      Llist:: Llist()
		{ head = NULL; 
		  sorted = NULL;
		}
	struct Node* Llist:: getHead()
		{ return head;
		}
	void Llist:: setHead(struct Node* headV)
		{ head = headV;
		}
	struct Node* Llist:: getSorted()
		{ return sorted;
		}

	void Llist:: display()
		{ 
		  struct Node *temp;
    		  if(head == NULL)
   		  {
        	   cout<<"The List is Empty"<<endl;
        	   return;
   		  }
    		  temp = head;
    		  cout<<"Elements of list are: "<<endl;
    		  while (temp != NULL)
    		  {
        	   cout<<temp->data<<"->";
        	   temp = temp->next;
   		  }
    		 cout<<"NULL"<<endl;
		}
	Node* Llist:: createNode(int value)
		{
		 struct Node *temp;
   		 temp = new(struct Node); 
    		 if (temp == NULL)
    		 {
       		   cout<<"Memory not allocated "<<endl;
        	   return 0;
   		 }
    		 else
    	 	 {
        	  temp->data = value;
        	  temp->next = NULL;     
        	  return temp;
    		 }
		}
	void Llist:: insertBegining()
                {
                  int value;
                  cout<<"Enter the value to be inserted: ";
                  cin>>value;
                  struct Node *temp;
                  temp = createNode(value);
		  struct Node* top;
		  if (head == NULL)
                  {
                   head = temp;
                   head->next = NULL;      
                  } 
                  else
                  {
                   top = head;
                   head = temp;
		   head->next = top;
                  }     
                 cout<<"Element Inserted at beginning"<<endl;
                }
	int Llist:: length()
		{
		  int count = 0;
   		  struct Node *current = head;
   		  while (current != NULL)
		  {
     		    count++;
      		    current = current->next;
  		  }
   		 return count;
		}
	int Llist:: count(int value)
		{
		  struct Node* current = head;
   		  int count = 0;
  		  while (current != NULL)
		  {
      		   if(current->data == value)
		    count++;
      		   current = current->next;
		  }
   		return count;
		}
	int Llist:: getNth(int pos)
		{
		 struct Node* current = head;
   		 int count = 0;  
   		 while (current != NULL)
		 {
      		  if (count == pos)
		  return (current->data);
      		  count++;
      		  current = current->next;
		 }
		 return 0;
		}
	void Llist:: delList()
		{
		 struct Node* current = head; 
   		 struct Node* next;
		
		 while (current != NULL)
		 {
		   next = current->next;
 		   free(current);
		   current = next;
	 	 }
		head = NULL;
		}
	int Llist:: pop()
		{
   		  int result;
   		  struct Node *temp;
   		  if(head != NULL)
		  {
   		   result = head->data; // store data at head node
   		   temp = head;
		   head = head->next;     // store next node
   		   free(temp);          
		   
   		   return(result);      
		  }
		  return 0;
		}
	void Llist:: insertAtPos(int pos)
		{
		  int l = length();
		  if(pos == 1)
			insertBegining();
		 
		  else if(pos>1 && pos<l)
		  {
		    cout<<"Enter value to be inserted at position "<<pos;
                    int val;
                    cin>>val;
		    struct Node *newN;
                    newN = createNode(val);
		    struct Node *current = head;
        	    for (int i = 0; i < pos-2; i++)
       		    {
           		if(current!=NULL)
			 current=current->next;
 		    }
		    if(current!=NULL)	
       		    { newN->next = current->next;
       		    current->next = newN;
		    }
		  }
		  else
		   {
			cout<<"Error : Out of bounds "<<endl;
		   }
		
		}
	void Llist:: sortedInsert(struct Node* newN)
		{ 
		  struct Node* current;
		  if (head== NULL || head->data >= newN->data)
   		  {
        	    newN->next = head;
        	    head = newN;
		  }
		  else
    	  	  {
     		   current = head;
        	   while (current->next!=NULL && current->next->data < newN->data)
       		   {
            	    current = current->next;
       		   }
		   newN->next = current->next;
        	   current->next = newN;
	          }
		 
		}
			
	void Llist:: insertionSort()
		{
		 Llist list2;
    		 struct Node *current = head;		//traverse LL and insert every node at sorted pos
    		 while (current != NULL)
   		 {
       		   struct Node *next = current->next;  //note down next node for next call to sortedinsert
        	   list2.sortedInsert(current);	//insert current value in sortedinsert
        	   current = next;			//update current node using previous value
   		 }

		 head = list2.getHead();				//make head point to sorted list			  
		}

	void Llist:: append(Llist list2)
		{ struct Node* headR;
		  headR = list2.getHead();
		  
		  if((head == NULL && headR == NULL) || headR==NULL)
			cout<<"Cannot append empty list"<<endl;
		  else if(head==NULL)
			{ cout<<"Attaching list onto empty list"<<endl;
			  head = headR;
			  display();	
			}
		  else
			{
			 struct Node* temp;
			 temp = head;
			 while(temp->next!=NULL)
			   {temp = temp-> next;}
			 temp->next = headR;
			 display();		
			}
			
		}
	void Llist:: frontBackSplit(Llist frontS, Llist backS)
		{
		  int len = length();
		  if(len<2)
		  { frontS.setHead(head);
		    backS.setHead(NULL);
		    return;
		  }
		  struct Node* current = head;
		  int hop = (len-1)/2;
		  for(int i=0; i<hop; i++)
		    current = current->next;
		  
		  frontS.setHead(head);
		  backS.setHead(current->next);
		  current->next = NULL;

		  frontS.display();
		  backS.display();
		}

	void Llist:: removeDuplicates()
		{
		  struct Node* current = head;

		  if (current == NULL) return;     // list is empty
					
		  while(current->next!=NULL)  // Compare current node with next node
   		  {	
			if (current->data == current->next->data) 
			{
      			  struct Node* temp = current->next->next;
      			  free(current->next);
      			  current->next = temp;
		  	}
			else
			{
			 current = current->next;
			}
		  }
		}
	void Llist:: moveNodes(Llist list2)
		{
		  struct Node* temp = list2.getHead();
		  if(temp==NULL)
 		    return;

		  list2.setHead(temp->next);
		  temp->next = head;
		  head = temp;
		
		  display();
		  list2.display();		  
		}
	void Llist:: alternatingSplit(Llist listA, Llist listB)
		{ 
		  struct Node* currNode,*headA,*headB;
		  currNode = head;
		  if (currNode == NULL || currNode->next == NULL) 
		  {
			return; 					// nothing to split, return
	   	  }
		  headA = currNode; 					//head for first Linked List
		  headB = currNode->next; 				//head for second Linked List
		  listA.setHead(headA);
		  listB.setHead(headB);
		  
		  while (currNode != NULL && currNode->next != NULL)
		  {
  			struct Node* t = currNode->next;
			currNode->next = t->next; 			// se next for first linked list
            
			if (currNode->next != NULL && currNode->next->next != NULL)
			{
				t->next = currNode->next->next;		 // set next for second linked list
			
			}
			else
			{ 				
				t->next = NULL;
				
			}
			currNode = currNode->next; 			
		   
		  }  

		       listA.display();
		       listB.display();
				  
 		}
	 void Llist:: shuffleMerge(Llist list1, Llist list2)
		{
		  struct Node *head1 = list1.getHead();
		  struct Node *head2 = list2.getHead();

		  struct Node *head1Next, *head2Next;
 
     
    		  while (head1 != NULL && head2 != NULL)
    		  {
        		 // Save next pointers
         		head1Next = head1->next;
         		head2Next = head2->next;
 
         		// Make head2 as next of head1
         		head2->next = head1Next;  // Change next pointer of head2
        		head1->next = head2;  // Change next pointer of head1
 
       			// Update current pointers for next iteration
        		 head1 = head1Next;
        		 head2 = head2Next;
    		  }
 
    		  list2.setHead(head2);

		  list1.display();
	 		  
		}

	Node* Llist:: sortedMerge(Node *headA, Node *headB)
		{
			

		    if(headA == NULL)
		    {
        		return headB;
   		    }
		    else if(headB == NULL)
		    {
        		return headA;
   		    }
    		 Node *head;
    		 if(headA->data < headB->data)
		 {
       			 head = headA;
       			 headA = headA->next;
   		 }
		 else
		 {
        		head = headB;
        		headB = headB->next;
   		 }
    		Node *current = head;
   		while(headA != NULL && headB != NULL)
		{
        	  if(headA->data < headB->data)
		  {
                   current->next = headA;
           	   headA = headA->next; 
        	  } 
		  else
		 {
            	   current->next = headB;
            	   headB = headB->next;           
       		 }
        	 current = current->next;
   	        }
   		if (headA == NULL)
		{
        	  current->next = headB;
   		}
		else
	        {
        	  current->next = headA;
   		}
    		return head;
		}
	Node* Llist::  mergeSort(struct Node* head)
		{
		  
		  Llist a,b;
   		  // if length 0 or 1
   		  if ((head == NULL) || (head->next == NULL))
		 {
		   return 0;
		 }
		
		 //Split head into 'a' and 'b' sublists
		  int len = length();
                  if(len<2)
                  { a.setHead(head);
                    b.setHead(NULL);
                    return 0;
                  }
                  struct Node* current = head;
                  int hop = (len-1)/2;
                  for(int i=0; i<hop; i++)
                    current = current->next;
                  
                  a.setHead(head);
                  b.setHead(current->next);
                  current->next = NULL;
                   
   		  struct Node* heada = a.getHead();
		  struct Node* headb = b.getHead();
		  mergeSort(heada); // Recursively sort the sublists
   		  mergeSort(headb);
   		  head  = sortedMerge(a.getHead(), b.getHead());
		  return head;
		}
		
		
int main()
{
  Llist list1,list2,frontS,backS,listA,listB,listC;
	
	int num,n,x,pos,ch=1,choice,length=0,counter=0,val=0;

	while (ch==1)
    {
        cout<<endl<<"---------------------------------"<<endl;
        cout<<endl<<"Operations on singly linked list"<<endl;
        cout<<endl<<"---------------------------------"<<endl;
        cout<<"1.Insert Node at beginning"<<endl;
        cout<<"2.Length of list"<<endl;
        cout<<"3.Count occurences of a particular node"<<endl;
        cout<<"4.Delete list"<<endl;
        cout<<"5.Pop node"<<endl;
        cout<<"6.Get value at position"<<endl;
        cout<<"7.Insert at position"<<endl;
        cout<<"8.Insert into sorted list"<<endl;
        cout<<"9.Insertion sort "<<endl;
	cout<<"10.Display list "<<endl;
	cout<<"11.Append list "<<endl;
	cout<<"12.Front Back split"<<endl;
	cout<<"13.Remove Duplicates"<<endl;
	cout<<"14.Move Node"<<endl;
	cout<<"15.Alternating split"<<endl;
	cout<<"16.Shuffle Merge"<<endl;
	cout<<"17.Sorted Merge"<<endl;
	cout<<"18.Sorted Intersect"<<endl;
	cout<<"19.Reverse"<<endl;
	cout<<"20.Recursive Reverse"<<endl;
        cout<<"21.Exit "<<endl;
        
	cout<<"Enter your choice : ";
        cin>>choice;
        if(choice<11)
	{ cout<<"Enter list number : ";
	  cin>>num;
	}
	switch(choice)
        {
        case 1:
            cout<<"Inserting Node at Beginning: "<<endl;
            if(num==1)
		list1.insertBegining();
            else 
		list2.insertBegining();
	    cout<<endl;
            break;
        case 2:
            cout<<"Length of node: ";
            if(num==1)
		length = list1.length();
            else
		length = list2.length();
	    cout<<length<<endl;
            break;
        case 3:
            cout<<"Count occurences of :";
	    cin>>n;
	    if(num==1)
            	counter = list1.count(n);
            else
		counter = list2.count(n);
	    cout<<counter<<endl;
            break;
        case 4:
            cout<<"Delete list"<<endl;
            if(num==1)
		list1.delList();
            else
		list2.delList();
		cout<<endl;
            break;
        case 5:
            cout<<"Popped node: ";
            if(num==1)
		x=list1.pop();
	    else
		x=list1.pop();
            cout<<x<<endl;
	    break;
        case 6:
            cout<<"Position:"<<endl;
	    cin>>pos;  
	    if(num==1)
		val = list1.getNth(pos-1);
            else
		val = list2.getNth(pos-1);
	    cout<<"Value :"<<val<<endl;
            break;
        case 7:
            cout<<"Insert at position: "<<endl;
	    cin>>pos;
	    if(num==1)
		list1.insertAtPos(pos);
            else
		list2.insertAtPos(pos);
	    cout<<endl;
            break;
        case 8:
            cout<<"Enter value to insert"<<endl;
            cin>>val;
	    struct Node *newN;
	    newN = list1.createNode(val);
	    if(num==1)
		{ newN = list1.createNode(val);
		  list1.sortedInsert(newN);
		}
	    else
		{ newN = list2.createNode(val);
		  list2.sortedInsert(newN);
		}
            cout<<endl;
            break;
        case 9:
            cout<<"Insertion sort of Link List"<<endl;
            if(num==1)
		list1.insertionSort();
	    else
		list2.insertionSort();
            cout<<endl;
            break;
	case 10:
	    if(num==1)
		list1.display();
	    else
		list2.display();
	    cout<<endl;
	    break;
	case 11:
	    
		list1.append(list2);
	   
	    cout<<endl;
	    break;
	case 12:
		
		  list1.frontBackSplit(frontS,backS);
		
		cout<<endl;
		break;
	case 13: 
		
		  list1.removeDuplicates();
		
		break;
	case 14: 
		
		  list1.moveNodes(list2);
	
		cout<<endl;
		break;
	case 15: listA.alternatingSplit(list1, list2);
		
		 cout<<endl;
		 break;
	case 16: listA.shuffleMerge(list1,list2);
		 
		 cout<<endl;
		 break;
	case 17: struct Node *headA,*headB,*headC;
		 headA = list1.getHead();
		 headB = list2.getHead();
		 headC = listA.sortedMerge(headA,headB);
		 listC.setHead(headC);
		 listC.display();
		 cout<<endl;
		 break;
	case 18:struct Node* head1;
		head1 = list1.getHead(); 
		head1 = list1.mergeSort(head1);
		list1.display();
		cout<<endl;
		break;
        case 21:
		cout<<"Exiting..."<<endl;
            	exit(1);
            	break;   
        default:
            cout<<"Wrong choice"<<endl;
        }
	cout<<"Press 1 to repear"<<endl;
	cin>>ch;
    }


return 0;
}
